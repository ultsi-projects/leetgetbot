FROM node:10.13.0

WORKDIR /app/
COPY package.json /app/
RUN npm install
RUN npm install typescript ts-node -g

RUN echo "Europe/Helsinki" > /etc/timezone
RUN ln -fs /usr/share/zoneinfo/`cat /etc/timezone` /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata

COPY . .
RUN tsc -p .

CMD ["node", "dist/index.js"]
