/*
    Leetgetbot, a leet get stats bot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    schema.sql
    Db schema, used to initialize db
*/

create table if not exists leet_streaks (
  id serial,
  chatId text,
  userId text,
  fromName text,
  streak int,
  best_streak int,
  updated timestamp with time zone not null default now(),
  primary key (id)
);
