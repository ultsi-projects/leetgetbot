/*
    Leetgetbot, a leet get stats bot for Telegram
    Copyright (C) 2018 Joonas Ulmanen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    index.ts
    Main entrypoint
*/

import TelegramBot, { Message } from 'node-telegram-bot-api'
import * as cron from 'node-cron'
import { updateStreaks, getUser, newStreak, getChatStreaks, getChatBestStreaks, updateAllStreakTimes } from './db';

const token = process.env.TOKEN || 'not set'
process.env.TOKEN = '***SECRET***'

if (token === 'not set') {
  throw new Error('TOKEN is not set!')
}

const bot = new TelegramBot(token, {polling: true})
const LEET_REGEX = /[l1][3e][3e][7t]/i
const CHRISTMAS_PEACE = false
const getters: {
  [chatId: string]: {userId: string, name: string}[]
} = {}
const userChatPairs: {
  [userChatPairId: string]: boolean
} = {}

function uniqueGetters(getters: {userId: string, name: string}[]) {
  const uniqGetters: {userId: string, name: string}[] = []

  for (const key in getters) {
    if (getters.hasOwnProperty(key)) {
      const newGetter = getters[key]
      let foundFlag = false
      for (const otherkey in uniqGetters) {
        if (uniqGetters.hasOwnProperty(otherkey)) {
          const uniqGetter = uniqGetters[otherkey]
          if (uniqGetter.userId === newGetter.userId && uniqGetter.name === newGetter.name) {
            foundFlag = true
            break
          } 
        }
      }
      if (!foundFlag) {
        uniqGetters.push(newGetter)
      }
    }
  }
  return uniqGetters
}

cron.schedule('38 13 * * *', async () => {
  try {
    if (CHRISTMAS_PEACE) {
      await updateAllStreakTimes()
      for (const chatId in getters) {
        if (getters.hasOwnProperty(chatId)) {
          const distinctGetters = uniqueGetters(getters[chatId])
          getters[chatId] = []
          await bot.sendMessage(chatId, `Hyvää joulurauhaa. Aika ajoin leetanneet: ${distinctGetters.map((getter) => getter.name).join(', ')}`)
        }
      }  
      return
    }

    for (const chatId in getters) {
      if (getters.hasOwnProperty(chatId)) {
        const distinctGetters = uniqueGetters(getters[chatId])
        await updateStreaks(chatId, distinctGetters.map(getter => getter.userId))
        getters[chatId] = []
      }

      const currentStreaks = await getChatStreaks(chatId)
      const bestStreaks = await getChatBestStreaks(chatId)

      const currentStreaksStr = currentStreaks.map(v => `${v.fromName}: ${v.streak} (${v.bestStreak})`).join('\n')
      const bestStreaksStr = bestStreaks.map((streaker) => `${streaker.fromName}`).join(', ')

      await bot.sendMessage(chatId, `Leetin aika on ohi. Streakit:\n\n${currentStreaksStr}\nTop (${bestStreaks[0].bestStreak} streak): ${bestStreaksStr}`)
    }
  } catch (err) {
    reportProblemToDev(err)
  }
})

bot.on('message', async (msg: Message) => {
  const msgSentDate = new Date(msg.date*1000)

  try {
    await initializeUserChatPair(msg)
  
    if(msgSentDate.getHours() === 13 && msgSentDate.getMinutes() === 37 || CHRISTMAS_PEACE) {
      await onLeetTime(msg)
    }
  } catch (err) {
    reportProblemToDev(err)
  }

})

function createUserChatId(msg: Message) {
  if(!msg.from) return ''
  return `${msg.from.id}${msg.chat.id}`
}

function formUsername(msg: Message) {
  if (!msg.from) return ''
  return msg.from.username || msg.from.first_name
}

async function initializeUserChatPair(msg: Message) {
  if (!msg.from) return
  const ucpId = createUserChatId(msg)
  const chatId = msg.chat.id.toString(),
        userId = msg.from.id.toString()
  
  if (!userChatPairs[ucpId]) {
    const userDetails = await getUser(userId, chatId)
    if (userDetails.rowCount === 0) {
      await newStreak(chatId,userId, formUsername(msg))
    }
    userChatPairs[ucpId] = true
  }
}

async function onLeetTime(msg: Message) {
  if (!msg.text || !msg.from || !msg.text.match(LEET_REGEX)) return
  const chatId = msg.chat.id.toString(),
        userId = msg.from.id.toString()
  const name = formUsername(msg)
  getters[chatId] = getters[chatId] || []
  getters[chatId].push({userId, name})
}

function reportProblemToDev(err: any) {
  console.log(err)
  bot.sendMessage(62461364, `Leetgetbot error: ${err}`)
}
